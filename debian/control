Source: libmail-imaptalk-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Angel Abad <angel@debian.org>,
           Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libauthen-sasl-perl <!nocheck>,
                     libencode-imaputf7-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libmail-imaptalk-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libmail-imaptalk-perl.git
Homepage: https://metacpan.org/release/Mail-IMAPTalk
Rules-Requires-Root: no

Package: libmail-imaptalk-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libauthen-sasl-perl,
         libencode-imaputf7-perl
Description: IMAP client interface with lots of features
 Mail::IMAPTalk communicates with an IMAP server. Each IMAP server command is
 mapped to a method of this object.
 .
 Although other IMAP modules exist on CPAN, this has several advantages over
 other modules.
  * It parses the more complex IMAP structures like envelopes and body
    structures into nice Perl data structures
  * It correctly supports atoms, quoted strings and literals at any point.
    Some parsers in other modules aren't fully IMAP compatiable and may
    break at odd times with certain messages on some servers
  * It allows large return values (eg. attachments on a message) to be read
    directly into a file, rather than into memory
  * It includes some helper functions to find the actual text/plain or
    text/html part of a message out of a complex MIME structure. It also
    can find a list of attachments, and CID links for HTML messages with.
    attached images
  * It supports decoding of MIME headers to Perl utf-8 strings
    automatically, so you don't have to deal with MIME encoded headers
    (enabled optionally)
 .
 While the IMAP protocol does allow for asynchronous running of commands, this
 module is designed to be used in a synchronous manner.
